package ghidterapeutic.controllers;

import ghidterapeutic.models.Section;
import ghidterapeutic.models.web.SectionFind;
import ghidterapeutic.services.SectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path="/section")
@CrossOrigin
public class SectionController {

    private SectionService sectionService;

    @Autowired
    public SectionController(SectionService sectionService)
    {
        this.sectionService = sectionService;
    }

    @PostMapping
    @Secured("ROLE_ADMIN")
    public @ResponseBody Section createSection (@Valid @RequestBody Section section)
    {
        return sectionService.save(section);
    }

    @PutMapping
    @Secured("ROLE_ADMIN")
    public @ResponseBody Section updateSection (@Valid @RequestBody Section section)
    {
        return sectionService.findAndSave(section);
    }

    @GetMapping
    public @ResponseBody Page<Section> getAllSections( @RequestParam int pathologyId, @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size)
    {
        Pageable pageObject = PageRequest.of(page, size);
        SectionFind sectionFind = new SectionFind(pathologyId);
        return sectionService.findAll(sectionFind, pageObject);
    }

    @GetMapping(path="/{sectionId}")
    public @ResponseBody Section getSection(@PathVariable (value = "sectionId") Integer sectionId)
    {
        return sectionService.findSectionById(sectionId);
    }
}
