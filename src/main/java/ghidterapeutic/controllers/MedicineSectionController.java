package ghidterapeutic.controllers;

import ghidterapeutic.models.MedicineSection;
import ghidterapeutic.models.web.MedicineSectionFind;
import ghidterapeutic.services.MedicineSectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path="/medicine-section")
@CrossOrigin
public class MedicineSectionController {
    private MedicineSectionService medicineSectionService;

    @Autowired
    public MedicineSectionController(MedicineSectionService medicineSectionService) {
        this.medicineSectionService = medicineSectionService;
    }

    @PostMapping
    @Secured("ROLE_ADMIN")
    public @ResponseBody
    MedicineSection createSection (@Valid @RequestBody MedicineSection medicineSection)
    {
        return medicineSectionService.save(medicineSection);
    }

    @PutMapping
    @Secured("ROLE_ADMIN")
    public @ResponseBody MedicineSection updateSection (@Valid @RequestBody MedicineSection medicineSection)
    {
        return medicineSectionService.findAndSave(medicineSection);
    }

    @GetMapping
    public @ResponseBody
    Page<MedicineSection> getAllSections(@RequestParam int medicineId, @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size)
    {
        Pageable pageObject = PageRequest.of(page, size);
        MedicineSectionFind medicineSectionFind = new MedicineSectionFind(medicineId);
        return medicineSectionService.findAll(medicineSectionFind, pageObject);
    }

    @GetMapping(path="/{medicineSectionId}")
    public @ResponseBody MedicineSection getSection(@PathVariable (value = "medicineSectionId") Integer medicineSectionId)
    {
        return medicineSectionService.findMedicineSectionById(medicineSectionId);
    }
}
