package ghidterapeutic.controllers;

import ghidterapeutic.models.Pathology;
import ghidterapeutic.models.Specialty;
import ghidterapeutic.models.web.PathologySearch;
import ghidterapeutic.services.SpecialtyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path="/specialty")
@CrossOrigin
public class SpecialtyController {
    private SpecialtyService specialtyService;

    @Autowired
    public SpecialtyController(SpecialtyService specialtyService) {
        this.specialtyService = specialtyService;
    }

    @PostMapping
    @Secured("ROLE_ADMIN")
    public @ResponseBody
    Specialty createSpecialty (@Valid @RequestBody Specialty specialty)
    {
        return specialtyService.save(specialty);
    }

    @PutMapping
    @Secured("ROLE_ADMIN")
    public @ResponseBody Specialty updateSpecialty (@Valid @RequestBody Specialty specialty)
    {
        return specialtyService.findAndSave(specialty);
    }

    @GetMapping
    public @ResponseBody
    Page<Specialty> getAllSpecialties(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size)
    {
        Pageable pageObject = PageRequest.of(page, size);
        return specialtyService.findAll(pageObject);
    }

    @GetMapping(path="/{specialtyId}")
    public @ResponseBody Specialty getSpecialty(@PathVariable (value = "specialtyId") Integer specialtyId)
    {
        return specialtyService.findSpecialtyById(specialtyId);
    }
}
