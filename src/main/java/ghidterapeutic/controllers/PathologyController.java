package ghidterapeutic.controllers;

import ghidterapeutic.models.Pathology;
import ghidterapeutic.models.web.PathologySearch;
import ghidterapeutic.services.PathologyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path="/pathology")
@CrossOrigin
public class PathologyController {

    private PathologyService pathologyService;

    @Autowired
    public PathologyController(PathologyService pathologyService)
    {
        this.pathologyService = pathologyService;
    }

    @PostMapping
    @Secured("ROLE_ADMIN")
    public @ResponseBody Pathology createPathology (@Valid @RequestBody Pathology pathology)
    {
        return pathologyService.save(pathology);
    }

    @PutMapping
    @Secured("ROLE_ADMIN")
    public @ResponseBody Pathology updatePathology (@Valid @RequestBody Pathology pathology)
    {
        return pathologyService.findAndSave(pathology);
    }

    @GetMapping
    public @ResponseBody Page<Pathology> getAllPathologies(@RequestParam(defaultValue = "") String name, @RequestParam(defaultValue = "0") String specialtyId, @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size)
    {
        Pageable pageObject = PageRequest.of(page, size);
        PathologySearch pathologySearch = new PathologySearch(name, Integer.parseInt(specialtyId));
        return pathologyService.findAll(pathologySearch, pageObject);
    }

    @GetMapping(path="/{pathologyId}")
    public @ResponseBody Pathology getPathology(@PathVariable (value = "pathologyId") Integer pathologyId)
    {
        return pathologyService.findPathologyById(pathologyId);
    }
}