package ghidterapeutic.controllers;

import ghidterapeutic.models.MedicinePackage;
import ghidterapeutic.models.MedicineSection;
import ghidterapeutic.models.web.MedicinePackageFind;
import ghidterapeutic.models.web.MedicineSectionFind;
import ghidterapeutic.services.MedicinePackageService;
import ghidterapeutic.services.MedicineSectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path="/medicine-package")
@CrossOrigin
public class MedicinePackageController {
    private MedicinePackageService medicinePackageService;

    @Autowired
    public MedicinePackageController(MedicinePackageService medicinePackageService) {
        this.medicinePackageService = medicinePackageService;
    }

    @PostMapping
    @Secured("ROLE_ADMIN")
    public @ResponseBody
    MedicinePackage createMedicinePackage (@Valid @RequestBody MedicinePackage medicinePackage)
    {
        return medicinePackageService.save(medicinePackage);
    }

    @PutMapping
    @Secured("ROLE_ADMIN")
    public @ResponseBody MedicinePackage updateMedicinePackage (@Valid @RequestBody MedicinePackage medicinePackage)
    {
        return medicinePackageService.findAndSave(medicinePackage);
    }

    @GetMapping
    public @ResponseBody
    Page<MedicinePackage> getAllMedicinePackages(@RequestParam int medicineId, @RequestParam(defaultValue = "") String codCIM, @RequestParam(defaultValue = "") String ambalaj, @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size)
    {
        Pageable pageObject = PageRequest.of(page, size);
        MedicinePackageFind medicinePackageFind = new MedicinePackageFind(medicineId, codCIM, ambalaj);
        return medicinePackageService.findAll(medicinePackageFind, pageObject);
    }

    @GetMapping(path="/{medicinePackageId}")
    public @ResponseBody MedicinePackage getSection(@PathVariable (value = "medicinePackageId") Integer medicinePackageId)
    {
        return medicinePackageService.findMedicinePackageById(medicinePackageId);
    }
}
