package ghidterapeutic.controllers;

import ghidterapeutic.models.Medicine;
import ghidterapeutic.models.web.MedicineSearch;
import ghidterapeutic.services.MedicineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path="/medicine")
@CrossOrigin
public class MedicineController {
    private MedicineService medicineService;

    @Autowired
    public MedicineController(MedicineService medicineService) {
        this.medicineService = medicineService;
    }

    @PostMapping
    @Secured("ROLE_ADMIN")
    public @ResponseBody
    Medicine createMedicine (@Valid @RequestBody Medicine medicine)
    {
        return medicineService.save(medicine);
    }

    @PutMapping
    @Secured("ROLE_ADMIN")
    public @ResponseBody Medicine updateMedicine (@Valid @RequestBody Medicine medicine)
    {
        return medicineService.findAndSave(medicine);
    }

    @GetMapping
    public @ResponseBody Page<Medicine> getAllMedicines(@RequestParam(defaultValue = "") String denumireComerciala, @RequestParam(defaultValue = "") String denumireComercialaEquals, @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size)
    {
        Pageable pageObject =  PageRequest.of(page, size);
        MedicineSearch medicineSearch = new MedicineSearch(denumireComerciala, denumireComercialaEquals);
        return medicineService.findAll(medicineSearch, pageObject);
    }

    @GetMapping(path="/{medicineId}")
    public @ResponseBody Medicine getMedicine(@PathVariable (value = "medicineId") Integer medicineId)
    {
        return medicineService.findMedicineById(medicineId);
    }
}
