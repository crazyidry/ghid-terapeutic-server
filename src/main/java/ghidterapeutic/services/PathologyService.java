package ghidterapeutic.services;

import ghidterapeutic.exceptions.ResourceNotFoundException;
import ghidterapeutic.helpers.StringHelper;
import ghidterapeutic.models.Pathology;
import ghidterapeutic.models.Specialty;
import ghidterapeutic.models.web.PathologySearch;
import ghidterapeutic.repositories.PathologyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class PathologyService {

    private PathologyRepository pathologyRepository;

    private SpecialtyService specialtyService;

    @Autowired
    public PathologyService(PathologyRepository pathologyRepository, SpecialtyService specialtyService) {
        this.pathologyRepository = pathologyRepository;
        this.specialtyService = specialtyService;
    }

    /**
     * Returns a Pathology based on the provided id
     * @param pathologyId
     * @return
     */
    public Pathology findPathologyById(Integer pathologyId)
    {
        return pathologyRepository.findPathologyById(pathologyId).orElseThrow(() -> new ResourceNotFoundException("Pathology", "id", pathologyId));
    }

    /**
     * Returns a Pathology based on the provided object
     * @param pathology
     * @return
     */
    public Pathology findPathology(Pathology pathology)
    {
        return pathologyRepository.findPathologyById(pathology.getId()).orElseThrow(() -> new ResourceNotFoundException("Pathology", "id", pathology.getId()));
    }

    /**
     * Saves a pathology
     * @param pathology
     * @return
     */
    public Pathology save(Pathology pathology)
    {
        pathology.setSlug(StringHelper.toSlug(pathology.getName()));
        mergeSpeciality(pathology, pathology.getSpecialty());

        return pathologyRepository.save(pathology);
    }

    /**
     * Updates a pathology
     * @param pathology
     * @return
     */
    public Pathology findAndSave(Pathology pathology)
    {
        return pathologyRepository.save(findAndMerge(pathology));
    }

    public Pathology findAndMerge(Pathology pathology)
    {
        Pathology updatedPathology = findPathologyById(pathology.getId());

        updatedPathology.setDescription(pathology.getDescription());
        updatedPathology.setName(pathology.getName());

        if (pathology.getSlug() != null) {
            updatedPathology.setSlug(pathology.getSlug());
        }

        mergeSpeciality(updatedPathology, pathology.getSpecialty());

        return updatedPathology;
    }

    /**
     * Merges the pathology's speciality
     * @param pathology
     * @param specialty
     */
    public void mergeSpeciality(Pathology pathology, Specialty specialty)
    {
        if (specialty != null) {
            specialty = specialtyService.findSpecialty(specialty);
            pathology.setSpecialty(specialty);
        }
    }

    /**
     * Returns all Pathologies
     * @param page
     * @return
     */
    public Page<Pathology> findAll(PathologySearch pathologySearch, Pageable page)
    {
        if (pathologySearch.getName().isEmpty() && pathologySearch.getSpecialtyId() == 0) {
            return pathologyRepository.findAll(page);
        }
        if (pathologySearch.getSpecialtyId() == 0) {
            return pathologyRepository.findByNameContaining(pathologySearch.getName(), page);
        }
        if (pathologySearch.getName().isEmpty()) {
            return pathologyRepository.findAllBySpecialty(specialtyService.findSpecialtyById(pathologySearch.getSpecialtyId()), page);
        }

        return pathologyRepository.findBySpecialtyAndNameContaining(specialtyService.findSpecialtyById(pathologySearch.getSpecialtyId()), pathologySearch.getName(), page);
    }
}
