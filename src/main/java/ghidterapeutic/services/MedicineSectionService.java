package ghidterapeutic.services;

import ghidterapeutic.exceptions.ResourceNotFoundException;
import ghidterapeutic.models.Medicine;
import ghidterapeutic.models.MedicineSection;
import ghidterapeutic.models.web.MedicineSectionFind;
import ghidterapeutic.repositories.MedicineSectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class MedicineSectionService {
    private MedicineSectionRepository medicineSectionRepository;

    private MedicineService medicineService;

    @Autowired
    public MedicineSectionService(MedicineSectionRepository medicineSectionRepository, MedicineService medicineService) {
        this.medicineSectionRepository = medicineSectionRepository;
        this.medicineService = medicineService;
    }

    /**
     * Returns a Medicine Section based on the provided id
     * @param medicineSectionId
     * @return
     */
    public MedicineSection findMedicineSectionById(Integer medicineSectionId)
    {
        return medicineSectionRepository.findMedicineSectionById(medicineSectionId).orElseThrow(() -> new ResourceNotFoundException("Medicine Section", "id", medicineSectionId));
    }

    /**
     * Create a new Medicine Section
     * @param medicineSection
     * @return
     */
    public MedicineSection save(MedicineSection medicineSection)
    {
        mergeMedicine(medicineSection, medicineSection.getMedicine());
        return medicineSectionRepository.saveAndFlush(medicineSection);
    }

    /**
     * Updates a Medicine Section
     * @param medicineSection
     * @return
     */
    public MedicineSection findAndSave(MedicineSection medicineSection)
    {
        return medicineSectionRepository.saveAndFlush(findAndMerge(medicineSection));
    }

    /**
     * Merges a Medicince Section
     * @param medicineSection
     * @return
     */
    public MedicineSection findAndMerge(MedicineSection medicineSection)
    {
        MedicineSection updatedMedicineSection = findMedicineSectionById(medicineSection.getId());

        updatedMedicineSection.setDescription(medicineSection.getDescription());
        updatedMedicineSection.setName(medicineSection.getName());
        mergeMedicine(updatedMedicineSection, medicineSection.getMedicine());


        return updatedMedicineSection;
    }

    /**
     * Merges the Medicine section's medicine
     * @param medicineSection
     * @param medicine
     */
    public void mergeMedicine(MedicineSection medicineSection, Medicine medicine)
    {
        if (medicine != null) {
            medicine = medicineService.findMedicine(medicine);
            medicineSection.setMedicine(medicine);
        }
    }

    /**
     * Find all medicine sections
     * @param medicineSectionFind
     * @param page
     * @return
     */
    public Page<MedicineSection> findAll(MedicineSectionFind medicineSectionFind, Pageable page)
    {
        return medicineSectionRepository.findAllByMedicine(medicineService.findMedicineById(medicineSectionFind.getMedicineId()), page);
    }
}
