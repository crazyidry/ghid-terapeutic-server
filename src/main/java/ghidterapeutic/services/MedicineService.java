package ghidterapeutic.services;

import ghidterapeutic.exceptions.ResourceNotFoundException;
import ghidterapeutic.helpers.StringHelper;
import ghidterapeutic.models.Medicine;
import ghidterapeutic.models.web.MedicineSearch;
import ghidterapeutic.repositories.MedicineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class MedicineService {

    private MedicineRepository medicineRepository;

    @Autowired
    public MedicineService(MedicineRepository medicineRepository) {
        this.medicineRepository = medicineRepository;
    }

    /**
     * Returns a Medicine based on the provided id
     * @param medicineId
     * @return
     */
    public Medicine findMedicineById(Integer medicineId)
    {
        return medicineRepository.findMedicineById(medicineId).orElseThrow(() -> new ResourceNotFoundException("Medicine", "id", medicineId));
    }

    /**
     * Returns a Medicine based on the provided object
     * @param medicine
     * @return
     */
    public Medicine findMedicine(Medicine medicine)
    {
        return medicineRepository.findMedicineById(medicine.getId()).orElseThrow(() -> new ResourceNotFoundException("Medicine", "id", medicine.getId()));
    }

    /**
     * Saves a medicine
     * @param medicine
     * @return
     */
    public Medicine save(Medicine medicine)
    {
        medicine.setSlug(StringHelper.toSlug(medicine.getDenumireComerciala()));

        return medicineRepository.save(medicine);
    }

    /**
     * Updates a medicine
     * @param medicine
     * @return
     */
    public Medicine findAndSave(Medicine medicine)
    {
        Medicine updatedMedicine = findMedicineById(medicine.getId());
        
        updatedMedicine.setActiuneTerapeutica(medicine.getActiuneTerapeutica());
        updatedMedicine.setCodATC(medicine.getCodATC());
        updatedMedicine.setConcentratie(medicine.getConcentratie());
        updatedMedicine.setDataActualizare(medicine.getDataActualizare());
        updatedMedicine.setDci(medicine.getDci());
        updatedMedicine.setDenumireComerciala(medicine.getDenumireComerciala());

        if (medicine.getSlug() != null) {
            updatedMedicine.setSlug(medicine.getSlug());
        }

        updatedMedicine.setDetinator(medicine.getDetinator());
        updatedMedicine.setFormaFarmaceutica(medicine.getFormaFarmaceutica());
        updatedMedicine.setPrescriptie(medicine.getPrescriptie());
        updatedMedicine.setProducator(medicine.getProducator());
        updatedMedicine.setStatusBDSTD(medicine.getStatusBDSTD());
        updatedMedicine.setRezumatCaracteristiciProdusUrl(medicine.getRezumatCaracteristiciProdusUrl());
        updatedMedicine.setProspectUrl(medicine.getProspectUrl());
        updatedMedicine.setAmbalajUrl(medicine.getAmbalajUrl());

        return medicineRepository.save(updatedMedicine);
    }

    /**
     * Returns all Medicines
     * @param page
     * @return
     */
    public Page<Medicine> findAll(MedicineSearch medicineSearch, Pageable page)
    {
        if (!medicineSearch.getDenumireComercialaEquals().isEmpty()) {
            return medicineRepository.findByDenumireComerciala(medicineSearch.getDenumireComercialaEquals(), page);
        }

        if (medicineSearch.getDenumireComerciala().isEmpty()) {
            return medicineRepository.findAll(page);
        }

        return medicineRepository.findByDenumireComercialaContaining(medicineSearch.getDenumireComerciala(), page);
    }
}
