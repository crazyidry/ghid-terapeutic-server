package ghidterapeutic.services;

import ghidterapeutic.exceptions.ResourceNotFoundException;
import ghidterapeutic.models.Medicine;
import ghidterapeutic.models.MedicinePackage;
import ghidterapeutic.models.web.MedicinePackageFind;
import ghidterapeutic.repositories.MedicinePackageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class MedicinePackageService {
    private MedicinePackageRepository medicinePackageRepository;

    private MedicineService medicineService;

    @Autowired
    public MedicinePackageService(MedicinePackageRepository medicinePackageRepository, MedicineService medicineService) {
        this.medicinePackageRepository = medicinePackageRepository;
        this.medicineService = medicineService;
    }

    /**
     * Returns a Medicine Package based on the provided id
     * @param medicinePackageId
     * @return
     */
    public MedicinePackage findMedicinePackageById(Integer medicinePackageId)
    {
        return medicinePackageRepository.findMedicinePackageById(medicinePackageId).orElseThrow(() -> new ResourceNotFoundException("Medicine Package", "id", medicinePackageId));
    }

    /**
     * Create a new Medicine Package
     * @param medicinePackage
     * @return
     */
    public MedicinePackage save(MedicinePackage medicinePackage)
    {
        mergeMedicine(medicinePackage, medicinePackage.getMedicine());
        return medicinePackageRepository.saveAndFlush(medicinePackage);
    }

    /**
     * Updates a Medicine Package
     * @param medicinePackage
     * @return
     */
    public MedicinePackage findAndSave(MedicinePackage medicinePackage)
    {
        return medicinePackageRepository.saveAndFlush(findAndMerge(medicinePackage));
    }

    /**
     * Merges a Medicine Package
     * @param medicinePackage
     * @return
     */
    public MedicinePackage findAndMerge(MedicinePackage medicinePackage)
    {
        MedicinePackage updatedMedicinePackage = findMedicinePackageById(medicinePackage.getId());

        updatedMedicinePackage.setAmbalaj(medicinePackage.getAmbalaj());
        updatedMedicinePackage.setCodCIM(medicinePackage.getCodCIM());
        updatedMedicinePackage.setNrDataAmbalajAPP(medicinePackage.getNrDataAmbalajAPP());
        updatedMedicinePackage.setVolumAmbalaj(medicinePackage.getVolumAmbalaj());
        updatedMedicinePackage.setValabilitate(medicinePackage.getValabilitate());

        mergeMedicine(updatedMedicinePackage, medicinePackage.getMedicine());


        return updatedMedicinePackage;
    }

    /**
     * Merges the Medicine package's medicine
     * @param medicinePackage
     * @param medicine
     */
    public void mergeMedicine(MedicinePackage medicinePackage, Medicine medicine)
    {
        if (medicine != null) {
            medicine = medicineService.findMedicine(medicine);
            medicinePackage.setMedicine(medicine);
        }
    }

    /**
     * Find all medicine packages
     * @param medicinePackageFind
     * @param page
     * @return
     */
    public Page<MedicinePackage> findAll(MedicinePackageFind medicinePackageFind, Pageable page)
    {
        if (!medicinePackageFind.getCodCIM().isEmpty() && !medicinePackageFind.getAmbalaj().isEmpty()) {
            return medicinePackageRepository.findAllByMedicineAndCodCIMAndAmbalaj(medicineService.findMedicineById(medicinePackageFind.getMedicineId()), medicinePackageFind.getCodCIM(), medicinePackageFind.getAmbalaj(), page);
        } else if (!medicinePackageFind.getCodCIM().isEmpty()) {
            return  medicinePackageRepository.findAllByMedicineAndCodCIM(medicineService.findMedicineById(medicinePackageFind.getMedicineId()), medicinePackageFind.getCodCIM(), page);
        } else if (!medicinePackageFind.getAmbalaj().isEmpty()) {
            return medicinePackageRepository.findAllByMedicineAndAmbalaj(medicineService.findMedicineById(medicinePackageFind.getMedicineId()), medicinePackageFind.getAmbalaj(), page);
        }
        return medicinePackageRepository.findAllByMedicine(medicineService.findMedicineById(medicinePackageFind.getMedicineId()), page);
    }
}
