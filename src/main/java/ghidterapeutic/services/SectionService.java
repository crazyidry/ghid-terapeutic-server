package ghidterapeutic.services;

import ghidterapeutic.exceptions.ResourceNotFoundException;
import ghidterapeutic.models.Pathology;
import ghidterapeutic.models.Section;
import ghidterapeutic.models.web.SectionFind;
import ghidterapeutic.repositories.SectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class SectionService {

    private SectionRepository sectionRepository;

    private PathologyService pathologyService;

    @Autowired
    public SectionService(SectionRepository sectionRepository, PathologyService pathologyService) {
        this.sectionRepository = sectionRepository;
        this.pathologyService = pathologyService;
    }

    /**
     * Returns a Section based on the provided id
     * @param sectionId
     * @return
     */
    public Section findSectionById(Integer sectionId)
    {
        return sectionRepository.findSectionById(sectionId).orElseThrow(() -> new ResourceNotFoundException("Section", "id", sectionId));
    }

    /**
     * Create a new Section
     * @param section
     * @return
     */
    public Section save(Section section)
    {
        mergePathology(section, section.getPathology());
        return sectionRepository.saveAndFlush(section);
    }

    /**
     * Updates a section
     * @param section
     * @return
     */
    public Section findAndSave(Section section)
    {
        return sectionRepository.saveAndFlush(findAndMerge(section));
    }

    /**
     * Merges a section
     * @param section
     * @return
     */
    public Section findAndMerge(Section section)
    {
        Section updatedSection = findSectionById(section.getId());

        updatedSection.setDescription(section.getDescription());
        updatedSection.setName(section.getName());
        mergePathology(updatedSection, section.getPathology());


        return updatedSection;
    }

    /**
     * Merges the section's pathology
     * @param section
     * @param pathology
     */
    public void mergePathology(Section section, Pathology pathology)
    {
        if (pathology != null) {
            pathology = pathologyService.findPathology(pathology);
            section.setPathology(pathology);
        }
    }

    /**
     * Find all sections
     * @param sectionFind
     * @return
     */
    public Page<Section> findAll(SectionFind sectionFind, Pageable page)
    {
        return sectionRepository.findAllByPathology(pathologyService.findPathologyById(sectionFind.getPathologyId()), page);
    }
}
