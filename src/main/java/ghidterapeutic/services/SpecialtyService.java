package ghidterapeutic.services;

import ghidterapeutic.exceptions.ResourceNotFoundException;
import ghidterapeutic.helpers.StringHelper;
import ghidterapeutic.models.Specialty;
import ghidterapeutic.repositories.SpecialtyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class SpecialtyService {
    private SpecialtyRepository specialtyRepository;

    @Autowired
    public SpecialtyService(SpecialtyRepository specialtyRepository) {
        this.specialtyRepository = specialtyRepository;
    }

    /**
     * Returns a Specialty based on the provided id
     * @param specialtyId
     * @return
     */
    public Specialty findSpecialtyById(Integer specialtyId)
    {
        return specialtyRepository.findSpecialtyById(specialtyId).orElseThrow(() -> new ResourceNotFoundException("Specialty", "id", specialtyId));
    }

    /**
     * Returns a Specialty based on the provided object
     * @param specialty
     * @return
     */
    public Specialty findSpecialty(Specialty specialty)
    {
        return specialtyRepository.findSpecialtyById(specialty.getId()).orElseThrow(() -> new ResourceNotFoundException("Specialty", "id", specialty.getId()));
    }

    /**
     * Saves a specialty
     * @param specialty
     * @return
     */
    public Specialty save(Specialty specialty)
    {
        specialty.setSlug(StringHelper.toSlug(specialty.getName()));

        return specialtyRepository.save(specialty);
    }

    /**
     * Updates a specialty
     * @param specialty
     * @return
     */
    public Specialty findAndSave(Specialty specialty)
    {
        return specialtyRepository.save(findAndMerge(specialty));
    }

    /**
     * Merge the Speciality with the one in the database
     * @param specialty
     * @return
     */
    public Specialty findAndMerge(Specialty specialty)
    {
        Specialty updatedSpecialty = findSpecialtyById(specialty.getId());

        updatedSpecialty.setName(specialty.getName());

        if (specialty.getSlug() != null) {
            updatedSpecialty.setSlug(specialty.getSlug());
        }

        return updatedSpecialty;
    }

    /**
     * Returns all Specialties
     * @param page
     * @return
     */
    public Page<Specialty> findAll(Pageable page)
    {
        return specialtyRepository.findAll(page);
    }
}
