package ghidterapeutic.repositories;

import ghidterapeutic.models.Medicine;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface MedicineRepository extends JpaRepository<Medicine, Integer>{
    Optional<Medicine> findMedicineById(@Param("id") Integer id);

    Page<Medicine> findByDenumireComercialaContaining(@Param("denumireComerciala") String denumireComerciala, Pageable page);

    Page<Medicine> findByDenumireComerciala(@Param("denumireComerciala") String denumireComerciala, Pageable page);
}
