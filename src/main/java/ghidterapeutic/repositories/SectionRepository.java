package ghidterapeutic.repositories;

import ghidterapeutic.models.Pathology;
import ghidterapeutic.models.Section;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface SectionRepository extends JpaRepository<Section, Integer> {
    Optional<Section> findSectionById(@Param("id") Integer id);

    Page<Section> findAllByPathology(@Param("pathology") Pathology pathology, Pageable page);
}
