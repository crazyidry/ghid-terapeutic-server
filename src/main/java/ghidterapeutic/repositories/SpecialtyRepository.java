package ghidterapeutic.repositories;

import ghidterapeutic.models.Specialty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface SpecialtyRepository extends JpaRepository<Specialty, Integer> {
    Optional<Specialty> findSpecialtyById(@Param("id") Integer id);
}
