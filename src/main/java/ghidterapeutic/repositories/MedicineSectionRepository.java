package ghidterapeutic.repositories;

import ghidterapeutic.models.Medicine;
import ghidterapeutic.models.MedicineSection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface MedicineSectionRepository extends JpaRepository<MedicineSection, Integer> {
    Optional<MedicineSection> findMedicineSectionById(@Param("id") Integer id);

    Page<MedicineSection> findAllByMedicine(@Param("medicine") Medicine medicine, Pageable page);
}
