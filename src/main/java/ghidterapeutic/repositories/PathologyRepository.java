package ghidterapeutic.repositories;

import ghidterapeutic.models.Pathology;
import ghidterapeutic.models.Specialty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface PathologyRepository extends JpaRepository<Pathology, Integer> {
    Optional<Pathology> findPathologyById(@Param("id") Integer id);

    Page<Pathology> findAllBySpecialty(@Param("speciality") Specialty speciality, Pageable page);

    Page<Pathology> findByNameContaining(@Param("name") String name, Pageable page);

    Page<Pathology> findBySpecialtyAndNameContaining(@Param("specialty") Specialty specialty, @Param("name") String name, Pageable page);

}