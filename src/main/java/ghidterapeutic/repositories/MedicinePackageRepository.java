package ghidterapeutic.repositories;

import ghidterapeutic.models.Medicine;
import ghidterapeutic.models.MedicinePackage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface MedicinePackageRepository extends JpaRepository<MedicinePackage, Integer> {
    Optional<MedicinePackage> findMedicinePackageById(@Param("id") Integer id);

    Page<MedicinePackage> findAllByMedicine(@Param("medicine") Medicine medicine, Pageable page);
    Page<MedicinePackage> findAllByMedicineAndCodCIM(@Param("medicine") Medicine medicine, @Param("codCIM") String codCIM, Pageable page);
    Page<MedicinePackage> findAllByMedicineAndAmbalaj(@Param("medicine") Medicine medicine, @Param("ambalaj") String ambalaj, Pageable page);
    Page<MedicinePackage> findAllByMedicineAndCodCIMAndAmbalaj(@Param("medicine") Medicine medicine, @Param("codCIM") String codCIM, @Param("ambalaj") String ambalaj, Pageable page);
}
