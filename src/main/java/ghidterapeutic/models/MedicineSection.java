package ghidterapeutic.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.sql.Timestamp;

@Entity
@Builder(toBuilder = true, builderClassName = "Builder")
@Table(name = "medicine_section")
@AllArgsConstructor
@NoArgsConstructor
public class MedicineSection {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @NotEmpty
    @Column(name = "name")
    private String name;

    @Column(name = "description")
    @Type(type = "text")
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="medicine_id", nullable=false)
    @JsonIgnore
    private Medicine medicine;

    @CreationTimestamp
    @Column(name = "created")
    @JsonIgnore
    private Timestamp created;

    @UpdateTimestamp
    @Column(name = "modified")
    @JsonIgnore
    private Timestamp modified;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Medicine getMedicine() {
        return medicine;
    }

    public void setMedicine(Medicine medicine) {
        this.medicine = medicine;
    }

    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public Timestamp getModified() {
        return modified;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }

    @JsonProperty("medicine")
    public void setMedicine(Integer medicineId) {
        if (medicine == null) {
            medicine = new Medicine();
        }
        medicine.setId(medicineId);
    }

    @JsonProperty("medicine")
    public Integer getMedicineId(){
        return medicine.getId();
    }
}
