package ghidterapeutic.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Builder(toBuilder = true, builderClassName = "Builder")
@Table(name = "medicine_package", indexes = {
        @Index(columnList = "ambalaj", name = "ambalaj_idx"),
        @Index(columnList = "codcim", name = "codcim_idx")
})
@AllArgsConstructor
@NoArgsConstructor
public class MedicinePackage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "codCIM")
    private String codCIM;

    @Column(name = "nrDataAmbalajAPP")
    private String nrDataAmbalajAPP;

    @Column(name = "ambalaj")
    private String ambalaj;

    @Column(name = "volumAmbalaj")
    private String volumAmbalaj;

    @Column(name = "valabilitate")
    private String valabilitate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="medicine_id", nullable=false)
    @JsonIgnore
    private Medicine medicine;

    @CreationTimestamp
    @Column(name = "created")
    @JsonIgnore
    private Timestamp created;

    @UpdateTimestamp
    @Column(name = "modified")
    @JsonIgnore
    private Timestamp modified;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodCIM() {
        return codCIM;
    }

    public void setCodCIM(String codCIM) {
        this.codCIM = codCIM;
    }

    public String getNrDataAmbalajAPP() {
        return nrDataAmbalajAPP;
    }

    public void setNrDataAmbalajAPP(String nrDataAmbalajAPP) {
        this.nrDataAmbalajAPP = nrDataAmbalajAPP;
    }

    public String getAmbalaj() {
        return ambalaj;
    }

    public void setAmbalaj(String ambalaj) {
        this.ambalaj = ambalaj;
    }

    public String getVolumAmbalaj() {
        return volumAmbalaj;
    }

    public void setVolumAmbalaj(String volumAmbalaj) {
        this.volumAmbalaj = volumAmbalaj;
    }

    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public Timestamp getModified() {
        return modified;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }

    public Medicine getMedicine() {
        return medicine;
    }

    public void setMedicine(Medicine medicine) {
        this.medicine = medicine;
    }

    @JsonProperty("medicine")
    public void setMedicine(Integer medicineId) {
        if (medicine == null) {
            medicine = new Medicine();
        }
        medicine.setId(medicineId);
    }

    @JsonProperty("medicine")
    public Integer getMedicineId(){
        return medicine.getId();
    }

    public String getValabilitate() {
        return valabilitate;
    }

    public void setValabilitate(String valabilitate) {
        this.valabilitate = valabilitate;
    }
}
