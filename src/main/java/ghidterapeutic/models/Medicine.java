package ghidterapeutic.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Builder(toBuilder = true, builderClassName = "Builder")
@Table(name = "medicine")
@AllArgsConstructor
@NoArgsConstructor
public class Medicine {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "denumireComerciala", nullable = false, unique = true)
    private String denumireComerciala;

    @Column(name = "slug", nullable = false, unique = true)
    private String slug;

    @Column(name = "DCI")
    private String dci;

    @Column(name = "formaFarmaceutica")
    private String formaFarmaceutica;

    @Column(name = "concentratie")
    private String concentratie;

    @Column(name = "producator")
    private String producator;

    @Column(name = "detinator")
    private String detinator;

    @Column(name = "codATC")
    private String codATC;

    @Column(name = "actiuneTerapeutica")
    private String actiuneTerapeutica;

    @Column(name = "prescriptie")
    private String prescriptie;

    @Column(name = "dataActualizare")
    private String dataActualizare;

    @OneToMany(targetEntity = MedicineSection.class, mappedBy = "medicine", orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private final List<MedicineSection> medicineSections = new ArrayList<>();

    @OneToMany(targetEntity = MedicinePackage.class, mappedBy = "medicine", orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private final List<MedicinePackage> medicinePackages = new ArrayList<>();

    @NotEmpty
    @Column(name = "statusBDSTD")
    private String statusBDSTD;

    @Column(name = "rezumatCaracteristiciProdusUrl")
    private String rezumatCaracteristiciProdusUrl;

    @Column(name = "prospectUrl")
    private String prospectUrl;

    @Column(name = "ambalajUrl")
    private String ambalajUrl;

    @CreationTimestamp
    @Column(name = "created")
    @JsonIgnore
    private Timestamp created;

    @UpdateTimestamp
    @Column(name = "modified")
    @JsonIgnore
    private Timestamp modified;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDenumireComerciala() {
        return denumireComerciala;
    }

    public void setDenumireComerciala(String denumireComerciala) {
        this.denumireComerciala = denumireComerciala;
    }

    public String getDci() {
        return dci;
    }

    public void setDci(String dci) {
        this.dci = dci;
    }

    public String getFormaFarmaceutica() {
        return formaFarmaceutica;
    }

    public void setFormaFarmaceutica(String formaFarmaceutica) {
        this.formaFarmaceutica = formaFarmaceutica;
    }

    public String getConcentratie() {
        return concentratie;
    }

    public void setConcentratie(String concentratie) {
        this.concentratie = concentratie;
    }

    public String getProducator() {
        return producator;
    }

    public void setProducator(String producator) {
        this.producator = producator;
    }

    public String getDetinator() {
        return detinator;
    }

    public void setDetinator(String detinator) {
        this.detinator = detinator;
    }

    public String getCodATC() {
        return codATC;
    }

    public void setCodATC(String codATC) {
        this.codATC = codATC;
    }

    public String getActiuneTerapeutica() {
        return actiuneTerapeutica;
    }

    public void setActiuneTerapeutica(String actiuneTerapeutica) {
        this.actiuneTerapeutica = actiuneTerapeutica;
    }

    public String getPrescriptie() {
        return prescriptie;
    }

    public void setPrescriptie(String prescriptie) {
        this.prescriptie = prescriptie;
    }

    public String getDataActualizare() {
        return dataActualizare;
    }

    public void setDataActualizare(String dataActualizare) {
        this.dataActualizare = dataActualizare;
    }

    public String getStatusBDSTD() {
        return statusBDSTD;
    }

    public void setStatusBDSTD(String statusBDSTD) {
        this.statusBDSTD = statusBDSTD;
    }

    public Timestamp getCreated() {
        return created;
    }

    public Timestamp getModified() {
        return modified;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    @JsonIgnore
    public List<MedicineSection> getMedicineSections() {
        return medicineSections;
    }

    @JsonIgnore
    public List<MedicinePackage> getMedicinePackages() {
        return medicinePackages;
    }

    public String getRezumatCaracteristiciProdusUrl() {
        return rezumatCaracteristiciProdusUrl;
    }

    public void setRezumatCaracteristiciProdusUrl(String rezumatCaracteristiciProdusUrl) {
        this.rezumatCaracteristiciProdusUrl = rezumatCaracteristiciProdusUrl;
    }

    public String getProspectUrl() {
        return prospectUrl;
    }

    public void setProspectUrl(String prospectUrl) {
        this.prospectUrl = prospectUrl;
    }

    public String getAmbalajUrl() {
        return ambalajUrl;
    }

    public void setAmbalajUrl(String ambalajUrl) {
        this.ambalajUrl = ambalajUrl;
    }
}
