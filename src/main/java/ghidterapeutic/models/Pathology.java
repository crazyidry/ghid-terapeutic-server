package ghidterapeutic.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.Path;
import javax.validation.constraints.NotEmpty;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity // This tells Hibernate to make a table out of this class
@Builder(toBuilder = true, builderClassName = "Builder")
@Table(name = "pathology")
@AllArgsConstructor
@NoArgsConstructor
public class Pathology {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @NotEmpty
    @Column(name = "name")
    private String name;

    @Column(name = "slug", nullable = false, unique = true)
    private String slug;

    @Column(name = "description")
    @Type(type = "text")
    private String description;

    @OneToMany(targetEntity = Section.class, mappedBy = "pathology", orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private final List<Section> sections = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="specialty_id", nullable=false)
    @JsonIgnore
    private Specialty specialty;

    @CreationTimestamp
    @Column(name = "created")
    @JsonIgnore
    private Timestamp created;

    @UpdateTimestamp
    @Column(name = "modified")
    @JsonIgnore
    private Timestamp modified;

    public Pathology(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    @JsonIgnore
    public List<Section> getSections() {
        return sections;
    }

    public Timestamp getCreated() {
        return created;
    }

    public Timestamp getModified() {
        return modified;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public Specialty getSpecialty() {
        return specialty;
    }

    @JsonProperty("specialty")
    public Integer getSpecialtyId() {
        return specialty.getId();
    }

    public void setSpecialty(Specialty specialty) {
        this.specialty = specialty;
    }

    @JsonProperty("specialty")
    public void setSpeciality(Integer specialityId)
    {
        if (specialty == null) {
            specialty = new Specialty(specialityId);
        }
        specialty.setId(specialityId);
    }
}