package ghidterapeutic.models.web;

public class MedicineSectionFind {
    private Integer medicineId;

    public MedicineSectionFind(Integer medicineId) {
        this.medicineId = medicineId;
    }

    public Integer getMedicineId() {
        return medicineId;
    }

    public void setMedicineId(Integer medicineId) {
        this.medicineId = medicineId;
    }
}
