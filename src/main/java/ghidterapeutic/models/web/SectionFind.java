package ghidterapeutic.models.web;

public class SectionFind {

    private Integer pathologyId;

    public SectionFind(Integer pathologyId) {
        this.pathologyId = pathologyId;
    }

    public Integer getPathologyId() {
        return pathologyId;
    }

    public void setPathologyId(Integer pathologyId) {
        this.pathologyId = pathologyId;
    }
}
