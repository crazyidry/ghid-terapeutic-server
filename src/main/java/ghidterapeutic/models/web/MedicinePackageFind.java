package ghidterapeutic.models.web;

public class MedicinePackageFind {
    private Integer medicineId;
    private String codCIM;
    private String ambalaj;

    public MedicinePackageFind(Integer medicineId, String codCIM, String ambalaj) {
        this.medicineId = medicineId;
        this.codCIM = codCIM;
        this.ambalaj = ambalaj;
    }

    public Integer getMedicineId() {
        return medicineId;
    }

    public void setMedicineId(Integer medicineId) {
        this.medicineId = medicineId;
    }

    public String getCodCIM() {
        return codCIM;
    }

    public void setCodCIM(String codCIM) {
        this.codCIM = codCIM;
    }

    public String getAmbalaj() {
        return ambalaj;
    }

    public void setAmbalaj(String ambalaj) {
        this.ambalaj = ambalaj;
    }
}
