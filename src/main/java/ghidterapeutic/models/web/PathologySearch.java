package ghidterapeutic.models.web;

public class PathologySearch {
    private String name;

    private Integer specialtyId;

    public PathologySearch(String name, Integer specialtyId) {
        this.name = name;
        this.specialtyId = specialtyId;
    }

    public Integer getSpecialtyId() {
        return specialtyId;
    }

    public void setSpecialtyId(Integer specialtyId) {
        this.specialtyId = specialtyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
