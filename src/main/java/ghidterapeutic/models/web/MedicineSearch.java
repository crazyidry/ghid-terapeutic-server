package ghidterapeutic.models.web;

public class MedicineSearch {
    private String denumireComerciala;
    private String denumireComercialaEquals;

    public MedicineSearch(String denumireComerciala, String denumireComercialaEquals) {
        this.denumireComerciala = denumireComerciala;
        this.denumireComercialaEquals = denumireComercialaEquals;
    }

    public String getDenumireComerciala() {
        return denumireComerciala;
    }

    public void setDenumireComerciala(String denumireComerciala) {
        this.denumireComerciala = denumireComerciala;
    }

    public String getDenumireComercialaEquals() {
        return denumireComercialaEquals;
    }

    public void setDenumireComercialaEquals(String denumireComercialaEquals) {
        this.denumireComercialaEquals = denumireComercialaEquals;
    }
}
