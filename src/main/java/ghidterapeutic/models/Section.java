package ghidterapeutic.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import java.sql.Timestamp;

@Entity
@Builder(toBuilder = true, builderClassName = "Builder")
@Table(name = "section")
@AllArgsConstructor
@NoArgsConstructor
public class Section {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @NotEmpty
    @Column(name = "name")
    private String name;

    @Column(name = "description")
    @Type(type = "text")
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="pathology_id", nullable=false)
    @JsonIgnore
    private Pathology pathology;

    @CreationTimestamp
    @Column(name = "created")
    @JsonIgnore
    private Timestamp created;

    @UpdateTimestamp
    @Column(name = "modified")
    @JsonIgnore
    private Timestamp modified;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Pathology getPathology() {
        return pathology;
    }

    @JsonProperty("pathology")
    public Integer getPathologyId() {
        return pathology.getId();
    }

    public Timestamp getCreated() {
        return created;
    }

    public Timestamp getModified() {
        return modified;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPathology(Pathology pathology) {
        this.pathology = pathology;
    }

    @JsonProperty("pathology")
    public void setPathology(Integer pathologyId) {
        if (pathology == null) {
            pathology = new Pathology(pathologyId);
        }
        pathology.setId(pathologyId);
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }
}
