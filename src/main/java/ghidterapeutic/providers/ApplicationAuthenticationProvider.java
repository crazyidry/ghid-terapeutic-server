package ghidterapeutic.providers;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import java.util.ArrayList;

public class ApplicationAuthenticationProvider implements AuthenticationProvider {
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String name = authentication.getName();
        String password = authentication.getCredentials().toString();
        if (authenticateUserPassword(name, password)) {
            return new UsernamePasswordAuthenticationToken(name, password, new ArrayList<>());
        } else {
            return null;
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

    public boolean authenticateUserPassword(String name, String password)
    {
        return "admin".equals(name) && "bUmAWJDlbqnI4jhbtz42%*SxLST#&yRtMT*KA6ttAve!2um4F0^T7Rvso*s&PNm!wXLBWay8^UuSsd4d^uOP!1v*$E1ExHdFbs6".equals(password);
    }
}
