package ghidterapeutic.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * ResourceNotFoundException class
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException {

    private String resourceName;
    private String fieldName;
    private Object fieldValue;

    /**
     * ResourceNotFoundException
     * @param resourceName
     * @param fieldName
     * @param fieldValue
     */
    public ResourceNotFoundException(String resourceName, String fieldName, Object fieldValue) {
        super(String.format("%s not found with %s : '%s'", resourceName, fieldName, fieldValue));
        this.resourceName = resourceName;
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
    }

    /**
     * getResourceName
     * @return string
     */
    public String getResourceName() {
        return resourceName;
    }

    /**
     * getFieldName
     * @return string
     */
    public String getFieldName() {
        return fieldName;
    }

    /**
     * getFieldValue
     * @return object
     */
    public Object getFieldValue() {
        return fieldValue;
    }
}
