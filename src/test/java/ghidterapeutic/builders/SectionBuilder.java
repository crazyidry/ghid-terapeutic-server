package ghidterapeutic.builders;

import ghidterapeutic.models.Pathology;
import ghidterapeutic.models.Section;

import java.sql.Timestamp;
import java.util.Date;

public class SectionBuilder {
    private Integer id = null;
    private String name = "";
    private String description = "";
    private Pathology pathology;
    private Timestamp created = new Timestamp(new Date().getTime());
    private Timestamp modified = new Timestamp(new Date().getTime());

    private Integer idIndex = 1;

    public static SectionBuilder anEmptySection()
    {
        return new SectionBuilder();
    }

    public Section build ()
    {
        Section section = new Section();

        section.setPathology(pathology);
        section.setDescription(description);
        section.setName(name);
        section.setId(id);
        section.setCreated(created);
        section.setModified(modified);

        return section;
    }

    public SectionBuilder withId(Integer id) {
        this.id = id;
        return this;
    }

    public SectionBuilder withNextId()
    {
        id = idIndex;
        idIndex++;
        return this;
    }

    public SectionBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public SectionBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public SectionBuilder withPathology(Pathology pathology) {
        this.pathology = pathology;
        return this;
    }

    public SectionBuilder createdOn(Timestamp created) {
        this.created = created;
        return this;
    }

    public SectionBuilder modifiedOn(Timestamp modified) {
        this.modified = modified;
        return this;
    }
}
