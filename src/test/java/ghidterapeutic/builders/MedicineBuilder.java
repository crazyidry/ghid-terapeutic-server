package ghidterapeutic.builders;

import ghidterapeutic.models.Medicine;

import java.sql.Timestamp;
import java.util.Date;

public class MedicineBuilder {
    private Integer id = null;
    private String denumireComerciala = "";
    private String dci = "";
    private String formaFarmaceutica = "";
    private String concentratie = "";
    private String producator = "";
    private String detinator = "";
    private String codATC = "";
    private String actiuneTerapeutica = "";
    private String prescriptie = "";
    private String valabilitate = "";
    private String dataActualizare = "";
    private String statusBDSTD = "";
    private String slug = "";
    private Timestamp created = new Timestamp(new Date().getTime());
    private Timestamp modified = new Timestamp(new Date().getTime());

    private Integer idIndex = 1;

    public static MedicineBuilder anEmptyMedicine() {
        return new MedicineBuilder();
    }

    public Medicine build()
    {
        Medicine medicine = new Medicine();

        medicine.setActiuneTerapeutica(actiuneTerapeutica);
        medicine.setCodATC(codATC);
        medicine.setConcentratie(concentratie);
        medicine.setDataActualizare(dataActualizare);
        medicine.setDci(dci);
        medicine.setDenumireComerciala(denumireComerciala);
        medicine.setDetinator(detinator);
        medicine.setFormaFarmaceutica(formaFarmaceutica);
        medicine.setPrescriptie(prescriptie);
        medicine.setProducator(producator);
        medicine.setStatusBDSTD(statusBDSTD);
        medicine.setId(id);
        medicine.setCreated(created);
        medicine.setModified(modified);
        medicine.setSlug(slug);

        return medicine;
    }

    public MedicineBuilder withId(Integer id) {
        this.id = id;
        return this;
    }

    public MedicineBuilder withNextId()
    {
        id = idIndex;
        idIndex++;
        return this;
    }

    public MedicineBuilder withDenumireComerciala(String denumireComerciala) {
        this.denumireComerciala = denumireComerciala;
        return this;
    }

    public MedicineBuilder withDci(String dci) {
        this.dci = dci;
        return this;
    }

    public MedicineBuilder withFormaFarmaceutica(String formaFarmaceutica) {
        this.formaFarmaceutica = formaFarmaceutica;
        return this;
    }

    public MedicineBuilder withConcentratie(String concentratie) {
        this.concentratie = concentratie;
        return this;
    }

    public MedicineBuilder withProducator(String producator) {
        this.producator = producator;
        return this;
    }

    public MedicineBuilder withDetinator(String detinator) {
        this.detinator = detinator;
        return this;
    }

    public MedicineBuilder withCodATC(String codATC) {
        this.codATC = codATC;
        return this;
    }

    public MedicineBuilder withActiuneTerapeutica(String actiuneTerapeutica) {
        this.actiuneTerapeutica = actiuneTerapeutica;
        return this;
    }

    public MedicineBuilder withPrescriptie(String prescriptie) {
        this.prescriptie = prescriptie;
        return this;
    }

    public MedicineBuilder withValabilitate(String valabilitate) {
        this.valabilitate = valabilitate;
        return this;
    }

    public MedicineBuilder withDataActualizare(String dataActualizare) {
        this.dataActualizare = dataActualizare;
        return this;
    }

    public MedicineBuilder withStatusBDSTD(String statusBDSTD) {
        this.statusBDSTD = statusBDSTD;
        return this;
    }

    public MedicineBuilder createdOn(Timestamp created) {
        this.created = created;
        return this;
    }

    public MedicineBuilder modifiedOn(Timestamp modified) {
        this.modified = modified;
        return this;
    }

    public MedicineBuilder withSlug(String slug) {
        this.slug = slug;
        return this;
    }
}
