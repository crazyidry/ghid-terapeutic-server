package ghidterapeutic.builders;

import ghidterapeutic.models.Pathology;
import ghidterapeutic.models.Section;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PathologyBuilder {
    private Integer id = null;
    private String name = "";
    private String slug = "";
    private String description = "";
    private final List<Section> sections = new ArrayList<>();
    private Timestamp created = new Timestamp(new Date().getTime());
    private Timestamp modified = new Timestamp(new Date().getTime());

    private Integer idIndex = 1;

    public static PathologyBuilder anEmptyPathology() {
        return new PathologyBuilder();
    }

    public Pathology build()
    {
        Pathology  pathology = new Pathology();

        pathology.setDescription(description);
        pathology.setId(id);
        pathology.setName(name);
        pathology.setCreated(created);
        pathology.setModified(modified);
        pathology.getSections().addAll(sections);

        return pathology;
    }

    public PathologyBuilder withId(Integer id) {
        this.id = id;
        return this;
    }

    public PathologyBuilder withNextId()
    {
        id = idIndex;
        idIndex++;
        return this;
    }

    public PathologyBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public PathologyBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public PathologyBuilder withSection(Section section)
    {
        sections.add(section);
        return this;
    }

    public PathologyBuilder withNoSection()
    {
        sections.clear();
        return this;
    }

    public PathologyBuilder createdOn(Timestamp created) {
        this.created = created;
        return this;
    }

    public PathologyBuilder modifiedOn(Timestamp modified) {
        this.modified = modified;
        return this;
    }

    public PathologyBuilder withSlug(String slug) {
        this.slug = slug;
        return this;
    }
}
