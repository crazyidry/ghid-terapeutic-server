package ghidterapeutic.services;

import ghidterapeutic.exceptions.ResourceNotFoundException;
import ghidterapeutic.models.Pathology;
import ghidterapeutic.models.Section;
import ghidterapeutic.models.web.SectionFind;
import ghidterapeutic.repositories.SectionRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static ghidterapeutic.builders.SectionBuilder.anEmptySection;
import static ghidterapeutic.builders.PathologyBuilder.anEmptyPathology;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SectionServiceTest {

    @MockBean
    private SectionRepository sectionRepository;

    @MockBean
    private PathologyService pathologyService;

    @Autowired
    private SectionService sectionService;

    private Pageable page1, page2;
    private Section emptySection;

    @Before
    public void setUp()
    {
        Pathology simplePathology = anEmptyPathology().withId(1).withName("Durere de cap").withDescription("Inflamatie in zona capului.").build();
        emptySection = anEmptySection().withPathology(simplePathology).build();

        Mockito.when(sectionRepository.findSectionById(1)).thenReturn(Optional.of(anEmptySection().withId(1).withName("Tratament").withDescription("Se da aspirina.").withPathology(simplePathology).build()));
        Mockito.when(sectionRepository.findSectionById(2)).thenReturn(Optional.empty());
        Mockito.when(sectionRepository.saveAndFlush(emptySection)).thenReturn(anEmptySection().withId(3).withPathology(simplePathology).build());
        Mockito.when(pathologyService.findPathology(simplePathology)).thenReturn(simplePathology);

        Section section = anEmptySection().withId(4).withName("Tratament").withPathology(simplePathology).build();
        Mockito.when(sectionRepository.saveAndFlush(section)).thenReturn(section);
        Mockito.when(sectionRepository.findSectionById(4)).thenReturn(Optional.of(section));

        page1 = new PageRequest(0, 10);
        page2 = new PageRequest(1, 10);

        List<Section> page1Response = new ArrayList<Section>();
        List<Section> page2Response = new ArrayList<Section>();
        page1Response.add(anEmptySection().withId(6).withPathology(simplePathology).build());
        page1Response.add(anEmptySection().withId(7).withPathology(simplePathology).build());

        Mockito.when(pathologyService.findPathologyById(1)).thenReturn(simplePathology);
        Mockito.when(pathologyService.findPathologyById(2)).thenThrow(new ResourceNotFoundException("Pathology", "id", 2));
        Mockito.when(sectionRepository.findAllByPathology(simplePathology, page2)).thenReturn(new PageImpl<Section>(page2Response, page2, 0));
        Mockito.when(sectionRepository.findAllByPathology(simplePathology, page1)).thenReturn(new PageImpl<Section>(page1Response, page1, 2));
        Mockito.when(sectionRepository.findAllByPathology(simplePathology, page2)).thenReturn(new PageImpl<Section>(page2Response, page2, 0));
        Mockito.when(sectionRepository.findAllByPathology(anEmptyPathology().withId(2).build(), page1)).thenReturn(new PageImpl<Section>(page2Response, page1, 0));
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testSectionNotFoundById()
    {
        sectionService.findSectionById(2);
    }

    @Test
    public void testSectionFoundById()
    {
        Section section = sectionService.findSectionById(1);

        Assert.assertEquals(Integer.valueOf(1), section.getId());
        Assert.assertEquals("Tratament", section.getName());
        Assert.assertEquals("Se da aspirina.", section.getDescription());
    }

    @Test
    public void testSave()
    {
        Section section = sectionService.save(emptySection);

        Assert.assertEquals(Integer.valueOf(3), section.getId());
    }

    @Test
    public void testFindAndSaveWithExistingEntity()
    {
        Section section = sectionService.findSectionById(4);
        section.setName("Recomandari");

        Section updatedSection = sectionService.findAndSave(section);

        Assert.assertEquals("Recomandari", updatedSection.getName());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testFindAndSaveWithNonExistingEntity()
    {
        Section section = anEmptySection().withId(5).withName("Recomandari").build();

        sectionService.findAndSave(section);
    }

    @Test
    public void testFindPage1All()
    {
        Page<Section> resp = sectionService.findAll(new SectionFind(1), page1);
        Assert.assertEquals(2, resp.getTotalElements());
    }

    @Test
    public void testFindPage2All()
    {
        Page<Section> resp = sectionService.findAll(new SectionFind(1), page2);
        Assert.assertEquals(0, resp.getTotalElements());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testFindPage1ButNoPathologyAll()
    {
        sectionService.findAll(new SectionFind(2), page1);
    }
}
