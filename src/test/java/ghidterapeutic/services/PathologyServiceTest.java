package ghidterapeutic.services;

import ghidterapeutic.exceptions.ResourceNotFoundException;
import ghidterapeutic.models.Pathology;
import ghidterapeutic.models.web.PathologySearch;
import ghidterapeutic.repositories.PathologyRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static ghidterapeutic.builders.PathologyBuilder.anEmptyPathology;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PathologyServiceTest {

    @MockBean
    private PathologyRepository pathologyRepository;

    @Autowired
    private PathologyService pathologyService;

    private Pageable page1, page2;
    private Pathology emptyPathology;
    private Pathology slugPathology;

    @Before
    public void setUp()
    {
        emptyPathology = anEmptyPathology().build();
        slugPathology = anEmptyPathology().build();

        Mockito.when(pathologyRepository.findPathologyById(1)).thenReturn(Optional.of(anEmptyPathology().withName("Raceala").withId(1).build()));
        Mockito.when(pathologyRepository.findPathologyById(2)).thenReturn(Optional.empty());
        Mockito.when(pathologyRepository.save(emptyPathology)).thenReturn(anEmptyPathology().withId(3).build());

        Pathology pathology = anEmptyPathology().withId(4).withName("Raceala").build();
        Mockito.when(pathologyRepository.save(pathology)).thenReturn(pathology);
        Mockito.when(pathologyRepository.findPathologyById(4)).thenReturn(Optional.of(pathology));

        page1 = new PageRequest(0, 10);
        page2 = new PageRequest(1, 10);

        List<Pathology> page1Response = new ArrayList<Pathology>();
        List<Pathology> page2Response = new ArrayList<Pathology>();
        page1Response.add(anEmptyPathology().withId(6).build());
        page1Response.add(anEmptyPathology().withId(7).build());

        Mockito.when(pathologyRepository.findAll(page1)).thenReturn(new PageImpl<Pathology>(page1Response, page1, 2));
        Mockito.when(pathologyRepository.findAll(page2)).thenReturn(new PageImpl<Pathology>(page2Response, page2, 0));

        Mockito.when(pathologyRepository.save(slugPathology)).thenReturn(slugPathology);
        Mockito.when(pathologyRepository.findPathologyById(8)).thenReturn(Optional.ofNullable(slugPathology));
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testPathologyNotFoundById()
    {
        pathologyService.findPathologyById(2);
    }

    @Test
    public void testPathologyFoundById()
    {
        Pathology pathology = pathologyService.findPathologyById(1);

        Assert.assertEquals(Integer.valueOf(1), pathology.getId());
        Assert.assertEquals("Raceala", pathology.getName());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testPathologyNotFound()
    {
        pathologyService.findPathology(anEmptyPathology().withId(2).build());
    }

    @Test
    public void testPathologyFound()
    {
        Pathology pathology = pathologyService.findPathology(anEmptyPathology().withId(1).build());

        Assert.assertEquals(Integer.valueOf(1), pathology.getId());
        Assert.assertEquals("Raceala", pathology.getName());
    }

    @Test
    public void testSave()
    {
        Pathology savedPathology = pathologyService.save(emptyPathology);

        Assert.assertEquals(Integer.valueOf(3), savedPathology.getId());
    }

    @Test
    public void testFindAndSaveWithExistingEntity()
    {
        Pathology pathology = pathologyService.findPathologyById(4);
        pathology.setName("O raceala");

        Pathology updatedPathology = pathologyService.findAndSave(pathology);

        Assert.assertEquals("O raceala", updatedPathology.getName());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testFindAndSaveWithNonExistingEntity()
    {
        Pathology pathology = anEmptyPathology().withId(5).withName("Luxatie").build();

        pathologyService.findAndSave(pathology);
    }

    @Test
    public void testFindPage1All()
    {
        PathologySearch pathologySearch = new PathologySearch("", 0);
        Page<Pathology> resp = pathologyService.findAll(pathologySearch, page1);
        Assert.assertEquals(2, resp.getTotalElements());
    }

    @Test
    public void testFindPage2All()
    {
        PathologySearch pathologySearch = new PathologySearch("", 0);
        Page<Pathology> resp = pathologyService.findAll(pathologySearch, page2);
        Assert.assertEquals(0, resp.getTotalElements());
    }

    @Test
    public void testSlugMedicine()
    {
        slugPathology.setName("Denumire de Test");
        Pathology pathology = pathologyService.save(slugPathology);
        Assert.assertEquals("denumire-de-test", pathology.getSlug());

        Pathology testPathology = anEmptyPathology().withId(8).build();
        pathology = pathologyService.findAndSave(testPathology);
        Assert.assertEquals("denumire-de-test", pathology.getSlug());

        testPathology.setSlug("denumire-de-test2");
        pathology = pathologyService.findAndSave(testPathology);
        Assert.assertEquals("denumire-de-test2", pathology.getSlug());
    }
}
