package ghidterapeutic.services;

import ghidterapeutic.exceptions.ResourceNotFoundException;
import ghidterapeutic.models.Medicine;
import ghidterapeutic.models.web.MedicineSearch;
import ghidterapeutic.repositories.MedicineRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static ghidterapeutic.builders.MedicineBuilder.anEmptyMedicine;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MedicineServiceTest {

    @MockBean
    private MedicineRepository medicineRepository;

    @Autowired
    private MedicineService medicineService;

    private Medicine emptyMedicine;
    private Medicine slugMedicine;
    private Pageable page1, page2;

    @Before
    public void setUp() {
        emptyMedicine = anEmptyMedicine().build();
        slugMedicine = anEmptyMedicine().build();

        Medicine medicine = anEmptyMedicine().withId(1).withDenumireComerciala("PARACETAMOL").build();
        page1 = new PageRequest(0, 10);
        page2 = new PageRequest(1, 10);

        Mockito.when(medicineRepository.findMedicineById(1)).thenReturn(Optional.ofNullable(medicine));
        Mockito.when(medicineRepository.findMedicineById(2)).thenReturn(Optional.empty());
        Mockito.when(medicineRepository.save(emptyMedicine)).thenReturn(anEmptyMedicine().withId(4).build());
        Mockito.when(medicineRepository.save(slugMedicine)).thenReturn(slugMedicine);
        Mockito.when(medicineRepository.findMedicineById(8)).thenReturn(Optional.ofNullable(slugMedicine));
        Mockito.when(medicineRepository.save(medicine)).thenReturn(medicine);

        List<Medicine> page1Response = new ArrayList<Medicine>();
        List<Medicine> page2Response = new ArrayList<Medicine>();
        page1Response.add(medicine);
        page1Response.add(emptyMedicine);

        Mockito.when(medicineRepository.findAll(page1)).thenReturn(new PageImpl<Medicine>(page1Response, page1, 2));
        Mockito.when(medicineRepository.findAll(page2)).thenReturn(new PageImpl<Medicine>(page2Response, page2, 0));
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testMedicineNotFound()
    {
        medicineService.findMedicineById(2);
    }

    @Test
    public void testMedicineFound()
    {
        Medicine medicine = medicineService.findMedicineById(1);

        Assert.assertEquals(Integer.valueOf(1), medicine.getId());
        Assert.assertEquals(medicine.getDenumireComerciala(),"PARACETAMOL");
    }

    @Test
    public void testSave()
    {
        Medicine medicine = medicineService.save(emptyMedicine);

        Assert.assertNotNull(medicine.getId());
        Assert.assertTrue(0 < medicine.getId());
    }

    @Test
    public void testFindAndSaveWithExistingEntity()
    {
        Medicine medicine = medicineService.findMedicineById(1);
        medicine.setDenumireComerciala("NUROFEN");

        Medicine updatedMedicine = medicineService.findAndSave(medicine);

        Assert.assertEquals(updatedMedicine.getDenumireComerciala(),"NUROFEN");
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testFindAndSaveWithNonExistingEntity()
    {
        Medicine medicine = anEmptyMedicine().withDenumireComerciala("IBUPROFEN").build();

        medicineService.findAndSave(medicine);
    }

    @Test
    public void testFindPage1All()
    {
        MedicineSearch medicineSearch = new MedicineSearch("", "");
        Page<Medicine> resp = medicineService.findAll(medicineSearch, page1);
        Assert.assertEquals(2, resp.getTotalElements());
    }

    @Test
    public void testFindPage2All()
    {
        MedicineSearch medicineSearch = new MedicineSearch("", "");
        Page<Medicine> resp = medicineService.findAll(medicineSearch, page2);
        Assert.assertEquals(0, resp.getTotalElements());
    }

    @Test
    public void testSlugMedicine()
    {
        slugMedicine.setDenumireComerciala("Denumire de Test");
        Medicine medicine = medicineService.save(slugMedicine);
        Assert.assertEquals("denumire-de-test", medicine.getSlug());
    }
}
