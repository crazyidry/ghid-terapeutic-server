## Create a new VM running the app and dependencies

1. cd deployment/infrastructure
2. ./build-computing-engine.sh
3. gcloud compute --project "ghid-terapeutic" ssh --zone {the_zone} "rest-api"
4. Inside the VM run ->

*sudo google_metadata_script_runner --script-type startup --debug*

5. The java application will fail connecting to the MySQL server but that is ok. 
We first have to create a new user and the database.
6. *sudo mysql_secure_installation* and follow the steps
7. sudo mysql -u root -p
8. GRANT ALL PRIVILEGES ON *.* TO 'live'@'localhost' IDENTIFIED BY 'pa55word';
9. GRANT ALL PRIVILEGES ON *.* TO 'live'@'127.0.0.1' IDENTIFIED BY 'pa55word';
10. CREATE DATABASE ghid_terapeutic;
11. quit
12. java -jar rest-api-0.1.0.jar &
13. ctrl + c 
14. ps aux | grep java -> should return something like ->

*idriosi+ 11704 35.4  5.9 2977676 224800 pts/1  Sl   19:09   0:21 java -jar rest-api-0.1.0.jar*

*idriosi+ 11749  0.0  0.0  12752   936 pts/1    S+   19:10   0:00 grep java*
 
Showing that the application is currently running 

## To deploy de app 

1. mvn clean package
2. cd deployment
3. ./deploy