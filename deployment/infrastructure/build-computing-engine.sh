#!/bin/sh

#Make sure we're in the current directory
building_computing_engine_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

source ./../get-properties.sh

APP_NAME=$(getProperty "app.name"           )
TEMPLATE=$(getProperty "app.source.template")

#Make sure we are in the current directory of the script
cd $building_computing_engine_path

#Spin new box
gcloud compute instances                                        \
       create  $APP_NAME                                        \
         --source-instance-template $TEMPLATE                   \
         --metadata-from-file startup-script=./startup-script.sh