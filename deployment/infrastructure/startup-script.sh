#!/bin/sh

# Install Java
sudo apt-get update
sudo apt-get -y --force-yes install openjdk-8-jdk

#Install htop
sudo apt-get install htop

#Certificate support
sudo apt install python-certbot-nginx

#Install MySQL
sudo apt-get -y install mysql-server

#Install NodeJS, NPM and serve [used to run the front end app]
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get -y install nodejs
sudo npm install -g serve

#Compression library needed to extract webapp build directory
sudo apt-get install bzip2

#Install nginx --> It would be a good idea to have the nginx configs on the cloud storage and pull them from there
sudo apt-get -y install nginx

# Make java 8 default
sudo update-alternatives --set java /usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java

# The the artifacts from google cloud storage
sudo gsutil cp gs://rest-api/rest-api-0.1.0.jar .

# Start server
sudo java -jar rest-api-0.1.0.jar