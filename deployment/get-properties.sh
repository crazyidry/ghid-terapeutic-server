#!/bin/bash

#Make sure we are in the current directory of the script
get_properties_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cd "$get_properties_path"

PROPERTY_FILE=app.properties

function getProperty {
   PROP_KEY=$1
   PROP_VALUE=`cat $PROPERTY_FILE | grep "$PROP_KEY" | cut -d'=' -f2`
   echo $PROP_VALUE
}
