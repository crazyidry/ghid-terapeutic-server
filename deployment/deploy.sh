#!/bin/bash

source ./get-properties.sh

APP_NAME=$(getProperty "app.name")
BUCKET_NAME=$(getProperty "bucket.name")

mvn -f ../pom.xml clean package

gsutil cp ../target/rest-api-0.1.0.jar gs://$BUCKET_NAME/opt/rest-api/rest-api-0.1.0.jar

gcloud compute ssh --zone europe-west4-a $APP_NAME --command "sudo gsutil cp gs://$BUCKET_NAME/opt/rest-api/rest-api-0.1.0.jar /opt/rest-api && sudo service rest-api restart"
