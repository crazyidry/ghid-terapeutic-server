#!/bin/bash

source ./get-properties.sh

APP_ADDRESS=$(getProperty "app.digitalocean.address")

mvn -f ../pom.xml clean package

echo "Clean and create server directories on $APP_ADDRESS ..."
ssh root@$APP_ADDRESS rm -rf /opt/rest-api/*
ssh root@$APP_ADDRESS mkdir /opt/rest-api

echo "Copy app files and run.sh script..."
scp ../target/rest-api-0.1.0.jar root@$APP_ADDRESS:/opt/rest-api/rest-api-0.1.0.jar
scp run.sh root@$APP_ADDRESS:/opt/rest-api/run.sh

echo "Restart the app.."
ssh root@$APP_ADDRESS service rest-api restart