#!/bin/python

from getmedicines import GetMedicines
from getpathologies import GetPathologies
from command import CommandManager

if __name__ == "__main__":
    cm = CommandManager()
    cm.add(GetMedicines)
    cm.add(GetPathologies)

    cm.execute()