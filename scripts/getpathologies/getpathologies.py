from command import Command

import os, requests
import json

class GetPathologies(Command):
    _name = "get-pathologies"
    _help = "Import all medicines from files"
    __listType = 0

    def _configure(self):
        self._parser.add_argument('--directory', type=str, help='Location of files')
        self._parser.add_argument('--api-location', type=str, help='Api location as http://localhost:8080')
        self._parser.add_argument('--api-user', type=str, help="The username for the api authentication")
        self._parser.add_argument('--api-password', type=str, help="The password for the api authentication")

    def _action(self, args):
        for subdir, dirs, files in os.walk(args.directory):
            for file in files:
                if file == ".DS_Store":
                    continue
                print file
                f=open("%s/%s" % (args.directory, file),'r')

                pathology = {}
                section = {}
                sections = []

                for line in f.readlines():
                    line = line.strip()
                    if not line:
                        continue
                    (processedLine, extra) = self.__process(line)
                    if line.startswith("## "):
                        pathology["name"] = processedLine
                        continue
                    if line.startswith("### "):
                        if "name" in section.keys():
                            section["description"] = "%s%s" % (section["description"], extra)
                            sections.append(section)
                            section = {}
                        section["name"] = line.lstrip("### ")
                        continue
                    if "name" not in section.keys():
                        if "description" not in section.keys():
                            pathology["description"] = ""
                        pathology["description"] = "%s%s" % (pathology["description"], processedLine)

                    if "description" not in section.keys():
                        section["description"] = ""
                    section["description"] = "%s%s" % (section["description"], processedLine)

                (processedLine, extra) = self.__process()
                section["description"] = "%s%s" % (section["description"], extra)
                sections.append(section)

                self.__save(pathology, sections, args.api_location, args.api_user, args.api_password)

                f.close()

    def __save(self, pathology, sections, api_location, api_user, api_password):
            resp = requests.post("%s/pathology" % (api_location), auth=(api_user, api_password), json=pathology)
            if resp.status_code != 200:
                print "Response Code: %s and Data: %s" % (resp.status_code, resp.text)
                return False

            pathology = resp.json()

            for section in sections:
                section["pathology"] = pathology["id"]
                print section
                resp = requests.post("%s/section" % (api_location), auth=(api_user, api_password), json=section)
                if resp.status_code != 200:
                    print "Response Code: %s and Data: %s" % (resp.status_code, resp.text)
                    return False

    def __process(self, line = None):
        extra = ""

        if line == None:
            extra = "</ul></li></ul>%s" % extra if self.__listType == 2 else extra
            extra = "</ul>%s" % extra if self.__listType == 1 else extra
            self.__listType = 0

            return ("", extra)

        stripLine = line.strip()
        if line.startswith("## "):
            stripLine = line.lstrip("## ")
            extra = "</ul></li></ul>%s" % extra if self.__listType == 2 else extra
            extra = "</ul>%s" % extra if self.__listType == 1 else extra
            self.__listType = 0
            return (stripLine, extra)

        if line.startswith("### "):
            stripLine = line.lstrip("### ")
            extra = "</ul></li></ul>%s" % extra if self.__listType == 2 else extra
            extra = "</ul>%s" % extra if self.__listType == 1 else extra
            self.__listType = 0
            return (stripLine, extra)

        if line.startswith("#### "):
            stripLine = "<h3>%s</h3>" % line.lstrip("#### ")
            stripLine = "</ul></li></ul>%s" % stripLine if self.__listType == 2 else stripLine
            stripLine = "</ul>%s" % stripLine if self.__listType == 1 else stripLine
            self.__listType = 0
            return (stripLine, extra)

        if line.startswith("- "):
            stripLine = line.lstrip("- ")
            stripLine = "<li>%s</li>" % stripLine if self.__listType == 1 else "<ul><li>%s</li>" % stripLine
            stripLine = "</ul></li>%s" % stripLine if self.__listType == 2 else stripLine
            self.__listType = 1
            return (stripLine, extra)

        if line.startswith("-- "):
            stripLine = line.lstrip("-- ")
            stripLine = "<li>%s</li>" % stripLine if self.__listType == 2 else "<ul><li>%s</li>" % stripLine
            stripLine = "<li>%s" % stripLine if self.__listType == 1 else stripLine
            stripLine = "<ul><li>%s" % stripLine if self.__listType == 0 else stripLine
            self.__listType = 2
            return (stripLine, extra)

        stripLine = "<p>%s</p>" % stripLine
        stripLine = "</ul></li></ul>%s" % stripLine if self.__listType == 2 else stripLine
        stripLine = "</ul>%s" % stripLine if self.__listType == 1 else stripLine
        self.__listType = 0

        return (stripLine, extra)