import argparse

class Command:
    _name = None
    _help = None

    def __init__(self, parser):
        self._parser = parser

    def _configure(self):
        pass

    def _action(self, args):
        pass

class CommandManager:
    def __init__(self):
        self.parser = argparse.ArgumentParser(prog='command')
        self.subparsers = self.parser.add_subparsers(title="commands", help='sub-command help')
        self.commands = {}

    def add(self, objectClass):
        subparser = self.subparsers.add_parser(objectClass._name, help=objectClass._help)
        object = objectClass(subparser)
        object._configure()
        subparser.set_defaults(command=object)

    def execute(self):
        args = self.parser.parse_args()
        args.command._action(args)