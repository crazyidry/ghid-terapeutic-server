from command import Command
import csv
import requests
import json
import codecs
import io
import urllib

headerToIndex = {}


class GetMedicines(Command):
    _name = "get-medicines"
    _help = "Import all medicines from CSV file"

    def _configure(self):
        self._parser.add_argument('--csv-file', type=str, help='Location of csv file')
        self._parser.add_argument('--api-location', type=str, help='Api location as http://localhost:8080')
        self._parser.add_argument('--api-user', type=str, help="The username for the api authentication")
        self._parser.add_argument('--api-password', type=str, help="The password for the api authentication")

    def _action(self, args):
        tableHeadToApiKeys = {
            "Cod CIM": "codCIM",
            "Denumire comerciala": "denumireComerciala",
            "DCI": "dci",
            "Forma farmaceutica": "formaFarmaceutica",
            "Concentratie": "concentratie",
            "Firma / tara producatoare APP": "producator",
            "Firma / tara detinatoare APP": "detinator",
            "Cod ATC": "codATC",
            "Actiune terapeutica": "actiuneTerapeutica",
            "Prescriptie": "prescriptie",
            "Nr / data ambalaj APP": "nrDataAmbalajAPP",
            "Ambalaj": "ambalaj",
            "Volum ambalaj": "volumAmbalaj",
            "Valabilitate ambalaj": "valabilitate",
            "Data actualizare": "dataActualizare"
        }

        with open(args.csv_file) as csvfile:
            reader = csv.reader(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            header = reader.next()

            elemId = 1
            for row in reader:
                print "Element %d" % (elemId)
                elemId += 1
                index = 0
                data = {}
                dataPackage = {}
                statusBDSTD = ""
                for item in row:
                    try:
                        if tableHeadToApiKeys[header[index]] in ["nrDataAmbalajAPP", "volumAmbalaj", "ambalaj", "codCIM", "valabilitate"]:
                            dataPackage[tableHeadToApiKeys[header[index]]] = item.strip()
                        else:
                            data[tableHeadToApiKeys[header[index]]] = item.strip()
                    except:
                        statusBDSTD += "0" if not item.strip() else "1"
                    index += 1
                data["statusBDSTD"] = statusBDSTD
                resp = requests.get("%s/medicine?denumireComercialaEquals=%s" % (args.api_location, urllib.quote(data["denumireComerciala"])))
                respData = resp.json()
                if respData["numberOfElements"] == 1:
                    data["id"] = respData["content"][0]["id"]
                    resp = requests.put("%s/medicine" % (args.api_location), auth=(args.api_user, args.api_password), json=data)
                else:
                    resp = requests.post("%s/medicine" % (args.api_location), auth=(args.api_user, args.api_password), json=data)

                if resp.status_code != 200:
                    print "Response Code: %s and Data: %s" % (resp.status_code, resp.text)
                    return False

                data = resp.json()
                dataPackage["medicine"] = data["id"]

                resp = requests.get("%s/medicine-package?medicineId=%s&ambalaj=%s" % (args.api_location, data["id"], urllib.quote(dataPackage["ambalaj"])))
                respData = resp.json()
                if respData["numberOfElements"] == 1:
                    dataPackage["id"] = respData["content"][0]["id"]
                    requests.put("%s/medicine-package" % (args.api_location), auth=(args.api_user, args.api_password), json=dataPackage)
                else:
                    requests.post("%s/medicine-package" % (args.api_location), auth=(args.api_user, args.api_password), json=dataPackage)