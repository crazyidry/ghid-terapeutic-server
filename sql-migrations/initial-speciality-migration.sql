insert into specialty (id, `name`, slug, created, modified) values
(1, 'Cardiologie', 'cardiologie', now(), now()),
(2, 'Urologie', 'urologie', now(), now()),
(3, 'ORL', 'orl', now(), now()),
(4, 'Pneumologie', 'pneumologie', now(), now()),
(5, 'Dermatologie', 'dermatologie', now(), now()),
(6, 'Gastroenterologie', 'gastroenterologie', now(), now()),
(7, 'Oftalmologie / Reumatologie', 'oftalmologie-reumatologie', now(), now());

delete from section where pathology_id in (21, 22, 23, 25, 26);

update pathology  set specialty_id = 3 where id > 0;

UPDATE `ghid_terapeutic`.`pathology` SET `specialty_id` = '1' WHERE (`id` = '2');
UPDATE `ghid_terapeutic`.`pathology` SET `specialty_id` = '1' WHERE (`id` = '1');
UPDATE `ghid_terapeutic`.`pathology` SET `specialty_id` = '1' WHERE (`id` = '3');
UPDATE `ghid_terapeutic`.`pathology` SET `specialty_id` = '1' WHERE (`id` = '20');
UPDATE `ghid_terapeutic`.`pathology` SET `specialty_id` = '1' WHERE (`id` = '19');
UPDATE `ghid_terapeutic`.`pathology` SET `specialty_id` = '2' WHERE (`id` = '4');
UPDATE `ghid_terapeutic`.`pathology` SET `specialty_id` = '2' WHERE (`id` = '17');
UPDATE `ghid_terapeutic`.`pathology` SET `specialty_id` = '4' WHERE (`id` = '28');
UPDATE `ghid_terapeutic`.`pathology` SET `specialty_id` = '4' WHERE (`id` = '11');
UPDATE `ghid_terapeutic`.`pathology` SET `specialty_id` = '6' WHERE (`id` = '5');
UPDATE `ghid_terapeutic`.`pathology` SET `specialty_id` = '6' WHERE (`id` = '7');
UPDATE `ghid_terapeutic`.`pathology` SET `specialty_id` = '6' WHERE (`id` = '8');
DELETE FROM `ghid_terapeutic`.`pathology` WHERE (`id` = '21');
DELETE FROM `ghid_terapeutic`.`pathology` WHERE (`id` = '22');
DELETE FROM `ghid_terapeutic`.`pathology` WHERE (`id` = '23');
DELETE FROM `ghid_terapeutic`.`pathology` WHERE (`id` = '25');
DELETE FROM `ghid_terapeutic`.`pathology` WHERE (`id` = '26');
UPDATE `ghid_terapeutic`.`pathology` SET `specialty_id` = '7' WHERE (`id` = '27');
UPDATE `ghid_terapeutic`.`pathology` SET `specialty_id` = '5' WHERE (`id` = '6');
UPDATE `ghid_terapeutic`.`pathology` SET `specialty_id` = '5' WHERE (`id` = '9');
UPDATE `ghid_terapeutic`.`pathology` SET `specialty_id` = '5' WHERE (`id` = '10');
UPDATE `ghid_terapeutic`.`pathology` SET `specialty_id` = '5' WHERE (`id` = '12');
UPDATE `ghid_terapeutic`.`pathology` SET `specialty_id` = '5' WHERE (`id` = '13');
UPDATE `ghid_terapeutic`.`pathology` SET `specialty_id` = '5' WHERE (`id` = '15');
UPDATE `ghid_terapeutic`.`pathology` SET `specialty_id` = '5' WHERE (`id` = '16');
UPDATE `ghid_terapeutic`.`pathology` SET `specialty_id` = '5' WHERE (`id` = '18');
