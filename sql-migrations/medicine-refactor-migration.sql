truncate table medicine;

ALTER TABLE `ghid_terapeutic`.`medicine`
    DROP COLUMN `volum_ambalaj`,
    DROP COLUMN `nr_data_ambalajapp`,
    DROP COLUMN `codcim`,
    DROP COLUMN `ambalaj`;

ALTER TABLE `ghid_terapeutic`.`medicine`
    DROP COLUMN `valabilitate`;

create unique index denumire_comerciala_idx on medicine(denumire_comerciala);
